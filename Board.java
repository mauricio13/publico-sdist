import java.util.HashMap;

public class Board {
	// The keys will be the client IP addresses
	private HashMap <String,Jugador> jugadores;
	private String lider;
	private int hitsToWin = 5;
	
	public Board(){
		jugadores = new HashMap<String,Jugador>();
	}
	
	public void registraJugador(String id){
		Jugador j = new Jugador(id);
		jugadores.put(id, j);
	}
	
	private void resetBoard(){
		jugadores.clear();
	}
	
	private void avisaFinDePartidaConGanador(String id){
		// Call the server class
	}
	
	public void remueveJugador(String id){
		jugadores.remove(id);
	}
	
	public boolean registraHit(String id){
		Jugador j = jugadores.get(id);
		
		// Si el jugador no esta registrado lo registra y despues incrementa su cuenta
		if (j == null) {
			registraJugador(id);
			j = jugadores.get(id);
		}
		
		// Jugador jugadorLider = jugadores.get(lider);
		
		// if(jugadorLider == null) lider = id;
		// else if (j.getHits() > jugadorLider.getHits()) lider = id;
		j.hit();
		
		if (j.getHits() == hitsToWin){
			resetBoard();
			return true;
		}
		
		return false;
		
	}
	
	
	public String getLider(){return lider;}

}
