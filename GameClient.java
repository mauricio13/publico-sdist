import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;


public class GameClient {
	
	private static int timeout = 2000;
	private static int width = 4;
	private static int height = 3;
	private static int inPort = 6787;
	private static int outPort = 6786;
	private static String mcAddress = "228.5.6.8";
	private static MulticastSocket s ;
	private static Socket outSocket;
	private static InetAddress group ;
	private static int id;

	public static void main(String[] args) throws InterruptedException, IOException {

        group = InetAddress.getByName(mcAddress);
        
        s = new MulticastSocket(inPort);
	   	s.joinGroup(group); 
	   	
	   	String idString = args[0];
	   	id = (idString != null) ? Integer.parseInt(idString) : 0;
	   	
	   	String  host = args[1];
	   	host = ( host != null) ? host : "localhost";
	   	
	   	// This comment is only here to test the git commiting bash
		while(true){
		   	byte[] buffer = new byte[1];
			DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
			System.out.println("Waiting next position");
			
			try {
				s.setSoTimeout(1000);
				s.receive(messageIn);
			} catch (IOException e){continue;}
			
			Integer msg = new Integer(messageIn.getData()[0]);
			int pos = msg.intValue();
			if(pos != -1){
			    printBoard(pos,width, height);
			    hit(id,pos,host);
			}
			else System.out.println("\n ========================================== \n Hubo un ganador reiniciando la partida \n ==========================================");
		}

	}
	
	public static int nextHitPosition(boolean userInteraction, int monsterPosition){
		
		if (userInteraction){
			Scanner u = new Scanner(System.in);
			System.out.println("El mounstro esta en la posici�n " + monsterPosition);
			System.out.println("Ingresa un posici�n para pegarle al mounstro");
			String next = u.next();
			return Integer.parseInt(next);
		}
		else {
			Random random  = new Random();
			int MAX_SLEEP_TIME = 2750;
			int timeToSleep = (int)(random.nextDouble()*MAX_SLEEP_TIME);
			
			try {Thread.sleep(timeToSleep);} catch (InterruptedException e) {}
			
	         // Hit the correct position with 50% chance
            int guess  = (random.nextBoolean()) ? monsterPosition : -1;
            if (id==0) guess = monsterPosition;
            return guess;
		}
	}
	
	public static boolean hit(int id, int monsterPosition,String host) throws InterruptedException, IOException{
		

		int succes = -2;
		boolean hit = false;
		
		try {
		    
			// Change of strategy let the client take as long as it wants,
			// Don't kill its thread
			
		    // This thread or connection must be killed before continuing 
		    // If the server time
		
		    outSocket = new Socket(host,outPort);
		    DataInputStream in = new DataInputStream(outSocket.getInputStream());
            DataOutputStream out = new DataOutputStream(outSocket.getOutputStream()); 
            // Hit the correct position with 50% chance
            // We need to ask user for input after this point
		    //int guess = nextHitPosition(false,pos); for simulation
		    int guess = nextHitPosition(true,monsterPosition);
		    
            int tmpGuess = guess;
            guess = empaquetaRespuesta(guess);
            
            out.writeInt(guess);
            // Read answer back from server
            System.out.println("Waiting response from server for guess " + tmpGuess);
           // System.out.printf("Before ---- %d\n",succes);
            succes = in.readInt();
           //  System.out.printf("After ---- %d\n",succes);
            switch (succes) {
            	case -2:
	            	hit = false;
	            	System.out.println("**** TIMEOUT ****");
	            	break;
            	case -1:
            		hit = false;
            		System.out.println("**** MISS ****");
            		break;
            	case 1:
            		hit = true;
            		System.out.println("**** HIT ****");
            		break;
            	case 2:
            		hit = true;
            		System.out.println("*********************************************************\n ***** FELICIDADES PARTIDA GANADA ***** \n*********************************************************");
            }
            
            if (succes == -1 && guess  >= 0){
            	System.out.println("**************************************************************");
            	System.out.println("**************************************************************");
            	System.out.println("********************** BIG PROBLEMA AMIGO ***********************");
            	System.out.println("**************************************************************");
            	System.out.println("**************************************************************");
            }

         }
    
         catch (IOException e){
            System.out.println("Te ganaron pelele");
         }
         
          return hit;
	}
	
    public static int empaquetaRespuesta(int respuesta){
        
        int negBit = 1 << 31;
         int idMask = negBit | 32767;
         respuesta = idMask & respuesta;
         int clientId = id << 16;
         respuesta = respuesta | clientId;
         System.out.println("Respuesta: " + respuesta);
        return respuesta;
        
    }
	
	public static void printBoard(int monster, int w, int h){
	    
	    int pos = 0;
	    for (int i = 0; i < h; i++){
	        for (int j = 0; j < w; j++){
	            if (pos  == monster)
	                System.out.print("X");
	            else System.out.print("O");
	            pos++;
	        }
	        System.out.println("");
	    }
	    
	}
	
}
