import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;



public class GameServer {

	private int width = 4;
	private int height = 3;
	public int nextPosition;
	private Random rand;
	private Board tablero;
	private int outPort = 6787;
	private int inPort = 6786;
	private String mcAddress = "228.5.6.8";
	private int timeout = 5000;
	private MulticastSocket s;
	private ServerSocket listenSocket;
	InetAddress group;
    
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	// This comment is only here to test the git commiting bash 2
	public void main() throws InterruptedException {
		// TODO Auto-generated method stub
        serverLoop();
}
	
	private void broadcastPositionWithSocket(int position, MulticastSocket channel){
		// Create the Output Message and broadcast to group
		Integer pos = new Integer(position);
		 byte [] m = new byte[1];
		 m[0] = pos.byteValue();
		System.out.println("Sending Message to everyone with pos: " + nextPosition);
	    DatagramPacket messageOut = new DatagramPacket(m, m.length, group, outPort);
	    try {
			s.send(messageOut);
		} catch (IOException e) {
			
		}
	}
	
	
	private void serverLoop() throws InterruptedException {
	    rand = new Random(123);
		tablero = new Board();
		
		try{
			group = InetAddress.getByName(mcAddress); 
			s = new MulticastSocket(outPort);
			s.joinGroup(group);
			
			while(true) {
				listenSocket = new ServerSocket(inPort);
				nextPosition = siguientePosicion();
				broadcastPositionWithSocket(nextPosition, s);
				Connection c = new Connection(listenSocket, this); // Create multiple;
				Timer t = new Timer();
				Timeout to = new Timeout(c);
				t.schedule(to, timeout);
			    c.join();
			    listenSocket.close();
			}
		} 
		catch(IOException e) {
		    System.out.println("Listen :"+ e.getMessage());
		    serverLoop();
		}
	}
	
	public int siguientePosicion(){
		int boardSize = width*height;
		return Math.abs(rand.nextInt() % boardSize);
	}
	
	
	
	public void restartConnection(ServerSocket connection){
	    try {
	        connection.close();
	        listenSocket = new ServerSocket(inPort);
	        
	    }
	    catch (Exception e){}
	}	
	
	public void publicaGanador(String id) throws IOException{
		// Create the Output Message
		byte [] m = new byte[1];
		m[0] = -1;
	    DatagramPacket messageOut = new DatagramPacket(m, m.length, group, outPort);
	    s.send(messageOut);
	    System.out.println("Tenemos un ganador " + id);
	    System.out.println("Reiniciando la partida");
	}
	
    public  int hit(String id, int position) throws IOException{
        boolean gano = false;
        int leDio = 0;
        
         int negBit = 1 << 31;
         int idMask = negBit | 32767;
        
        if ((position & idMask) == nextPosition){
                id = id + (position >> 16);
                gano = tablero.registraHit(id);
                leDio = 1;
        }
        
        if(gano) {
        	publicaGanador(id);
        	leDio = 2;
        }
        return leDio;
}

}


class Timeout extends TimerTask {

	Connection connectionToTimeout;
	
	public Timeout(Connection c){
		connectionToTimeout = c;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		// Close connections and send new coordinates
		connectionToTimeout.timeoutClient();
		System.out.println("**** Run out of time *****");
		
	}
	
}


class Connection extends Thread {

    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;
    ServerSocket serverSocket;
    GameServer server;
    int timeout = 5000;
    boolean inTime = true;
    
    public Connection (ServerSocket aServerSocket, GameServer server) {
    	System.out.println("Opening a connection with the clients");
    	serverSocket = aServerSocket;
    	this.server = server;
    	this.start();
    }
    
    public void timeoutClient(){
    	try {
    		System.out.println("Telling the client its time is up");
    		if (out != null) {
    			out.writeInt(-2);
    			inTime = false;
    		}
			closeConnection();
		} catch (IOException e) {

		}
    }
    
    public void closeConnection(){

    }
    
    public void run() {
    	try {
    		System.out.println("Inside connection loop");
    		serverSocket.setSoTimeout(timeout);
    		clientSocket = serverSocket.accept();
    		in = new DataInputStream( clientSocket.getInputStream());
    		out = new DataOutputStream( clientSocket.getOutputStream());
            int hitGuess = in.readInt();
            if (inTime){
            	
            	int negBit = 1 << 31;
                int idMask = negBit | 32767;
                hitGuess &= idMask;
                
	            System.out.println("The client guessed "+hitGuess + " the correct choice was " + server.nextPosition);
	            int leDio = server.hit(clientSocket.getInetAddress().toString(),hitGuess);
	            if (leDio == 1) {
	            	System.out.println("Telling client it was a  ** HIT **");
	            	out.writeInt(1);
	            }
	            else if (leDio == 2){
	            	System.out.println("Telling client it won the match  ** WIN **");
	            	out.writeInt(2);
	            }
	            else  {
	            	System.out.println("Telling client it was a  ** MISS **");
	            	out.writeInt(-1);
	            	// Open a new connection so other clients can guess
	            	Connection c = new Connection(serverSocket, server);
	            	try {
						c.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        } 
    	}
        catch(IOException e1) {
        	System.out.println("Timeout");
    }
      
}
}

