
public class Jugador {
	private String id;
	private int hits;

	public Jugador(String id){
		this.id = id;
		hits = 0;
	}
	
	public String getId(){return id;}
	
	public void setHits(int hits){this.hits = hits;}
	
	public void hit(){hits += 1;}
	
	public int getHits(){return hits;}
	
	
}
