import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;


public class GameClient {
	
	private static int timeout = 2000;
	private static int width = 4;
	private static int height = 3;
	private static int inPort = 6787;
	private static int outPort = 6786;
	private static String mcAddress = "228.5.6.8";
	private static MulticastSocket s ;
	private static Socket outSocket;
	private static InetAddress group ;

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
        group = InetAddress.getByName(mcAddress); 
		
        s = new MulticastSocket(inPort);
	   	s.joinGroup(group); 
	   	int i = 0;
		while(true /*i < 7*/){
		   	byte[] buffer = new byte[1];
			DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
			System.out.println("Waiting next position");
			// The server must be up before the client is run, oherwise this line of code will fail 
			// it also blocks on the caller
			try {
				s.setSoTimeout(1250);
				s.receive(messageIn);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				continue;
			}
			Integer msg = new Integer(messageIn.getData()[0]);
			int pos = msg.intValue();
			if(pos != -1){
			    printBoard(pos,width, height);
			    hit(pos,i);
			}
			else System.out.println("\n ========================================== \n Hubo un ganador reinicializando la partida \n ==========================================");
		}
		//i++;

	}
	
	public static boolean hit(int position,int t) throws InterruptedException, UnknownHostException{
		
		Random random  = new Random();
		int MAX_SLEEP_TIME = 2750;
		int timeToSleep = (int)(random.nextDouble()*MAX_SLEEP_TIME);
		if (t == 2) timeToSleep = 0;
		boolean succes = false;
		
		
		try {
		    
		    // This thread or connection must be killed before continuing 
		    // If the server time
		
		    outSocket = new Socket("localhost",outPort);
		    DataInputStream in = new DataInputStream(outSocket.getInputStream());
            DataOutputStream out = new DataOutputStream(outSocket.getOutputStream()); 
            outSocket.setSoTimeout(timeout);
            // Hit the correct position with 50% chance
         
            int guess  = (random.nextBoolean()) ? position : -1;
            Thread.sleep(timeToSleep);
            out.writeInt(guess);
            // Read answer back from server
            System.out.println("Waiting response from server for guess " + guess);
            succes = in.readBoolean();
            if(succes) System.out.println("HIT");
            else System.out.println("MISS");

         }
         
         catch (IOException e){
            System.out.println("Timeout");
         }
         
          return succes;
	}
	
	public static void printBoard(int monster, int w, int h){
	    
	    int pos = 0;
	    for (int i = 0; i < h; i++){
	        for (int j = 0; j < w; j++){
	            if (pos  == monster)
	                System.out.print("X");
	            else System.out.print("O");
	            pos++;
	        }
	        System.out.println("");
	    }
	    
	}
	
}
