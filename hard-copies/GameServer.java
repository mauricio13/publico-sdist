import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Random;



public class GameServer {

	private int width = 4;
	private int height = 3;
	public int nextPosition;
	private Random rand;
	private int clientes = 0;
	private Board tablero;
	private HashSet<String> clientAddresses;
	private int outPort = 6787;
	private int inPort = 6786;
	private String mcAddress = "228.5.6.8";
	private int timeout = 2000;
	private MulticastSocket s;
	private ServerSocket listenSocket;
	InetAddress group;
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public void main() throws InterruptedException {
		// TODO Auto-generated method stub
        serverLoop();
}

	
	public void serverLoop() throws InterruptedException {
	    rand = new Random(123);
		tablero = new Board();
		clientAddresses = new HashSet<String>();
		
		try{
			group = InetAddress.getByName(mcAddress); 
			s = new MulticastSocket(outPort);
			s.joinGroup(group);
			boolean first = true;
			int i = 0;
			listenSocket = new ServerSocket(inPort);
			
			while(true /*i < 8*/) {
                // obtén la posición del monstro
				nextPosition = siguientePosicion();
				Integer pos = new Integer(nextPosition);
				// Create the Output Message
				 byte [] m = new byte[1];
				 m[0] = pos.byteValue();
				 // send it 
				System.out.println("Sending Message to everyone with pos: " + nextPosition);
			    DatagramPacket messageOut = new DatagramPacket(m, m.length, group, outPort);
			    s.send(messageOut);
			    // Set a timeout window
			    
			    // After sending the message we need to wait for clients responses using TCP
			    
			    
			 
			    //listenSocket.setSoTimeout(timeout);
			    // Define an inner loop to allow for multiple guesses and connections from more than one player
			    // This right here blocks so we need to put thi on the thread
			    System.out.println("Opening a connection with the clients");
			    Connection c =  new Connection(listenSocket, this);
			    // Timeout in here
			    c.join();
			    //i++;
			    
			}
			
		} 
		catch(IOException e) {
		    System.out.println("Listen :"+ e.getMessage());
		    serverLoop();
		}
	    
	}
	
	public int siguientePosicion(){
		int boardSize = width*height;
		return Math.abs(rand.nextInt() % boardSize);
	}
	
	public void registraCliente(){
		clientes++;
	}
	
	public  void desconectaClient(int id){
		
	}
	
	public void restartConnection(ServerSocket connection){
	    try {
	        connection.close();
	        listenSocket = new ServerSocket(inPort);
	        
	    }
	    catch (Exception e){}
	}	
	public void publicaGanador(String id) throws IOException{
		Integer pos = new Integer(nextPosition);
		// Create the Output Message
		byte [] m = new byte[1];
		m[0] = -1;
	    DatagramPacket messageOut = new DatagramPacket(m, m.length, group, outPort);
	    s.send(messageOut);
	    System.out.println("Tenemos un ganador " + id);
	}
	
	public  boolean hit(String id, int position) throws IOException{
		boolean gano = false;
		boolean leDio = false;
		if (position == nextPosition){
			gano = tablero.registraHit(id);
			leDio = true;
		}
		
		if(gano) publicaGanador(id);
		return leDio;
	}

}

class Connection extends Thread {

    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;
    ServerSocket serverSocket;
    GameServer server;
    int timeout = 2000;
    int time = 0;
    
    public Connection (ServerSocket aServerSocket, GameServer server) {
    	serverSocket = aServerSocket;
    	this.server = server;
    	this.start();
    }
    public void run() {
    	try {
    		System.out.println("In loop for " + time + " time");
    		// Put the timeout in here
    		serverSocket.setSoTimeout(timeout);
    		clientSocket = serverSocket.accept();
    		//clientSocket.setSoTimeout(timeout);
    		in = new DataInputStream( clientSocket.getInputStream());
    		out = new DataOutputStream( clientSocket.getOutputStream());
    		in.skip(in.available());
            int hitGuess = in.readInt(); 
            System.out.println("the client guessed "+hitGuess + " the correct choice was " + server.nextPosition);
            boolean leDio = server.hit(clientSocket.getInetAddress().toString(),hitGuess);
            if (leDio) System.out.println("Telling client it was a  ** HIT **");
            else  System.out.println("Telling client it was a  ** MISS **");
            out.writeBoolean(leDio);
            time++;
        } 
        catch(IOException e1) {
            System.out.println("** Timeout **");
            server.restartConnection(serverSocket);
    }
      
}
}

