

rt java.util.ArrayList;
    
public class Parser {
    public static void main(String args[]){
    
        String tweet = "This is a tweet @myself whit a #hash and someone @else #hasht #hashc";
        ArrayList <Tag> tags = getTagsFromTweet(tweet);
        for (Tag tag : tags){
            System.out.println(tag.toString());
        }
    }

    public static ArrayList <Tag> getTagsFromTweet(String tweet){
        int tweetLength = tweet.length();
        ArrayList <Tag> tags = new ArrayList <Tag>();
        for(int i = 0; i < tweetLength; i++){
            char current = tweet.charAt(i);
            if (current == '@'){
                StringBuilder s = new StringBuilder();
                current = tweet.charAt(i);
                while(i < tweetLength - 1 && current != ' ' && current != '\n'){
                    i++;
                    current = tweet.charAt(i);
                    s.append(current);
                }
                Tag t = new Tag(Tag.TagType.ATTAG, s.toString());
                tags.add(t);
            }
            if (current == '#'){
                StringBuilder s = new StringBuilder();
                current = tweet.charAt(i);
                while(i < tweetLength - 1  && current != ' ' && current != '\n'){
                    i++;
                    current = tweet.charAt(i);  
                    s.append(current);
                }
                tags.add(new Tag(Tag.TagType.HASHTAG, s.toString()));
            }
        }
        
        return tags;

    }
    
    public String word(String substring){
        substring.split(" |\n",1);
        return "";
    }
}


class Tag {
    public static  enum TagType {HASHTAG, ATTAG}
        
        public TagType type;
        public String tag;

        public Tag(TagType type, String tag){
            this.type = type;
            this.tag = tag;
        } 

        public String toString(){
            if(type == TagType.HASHTAG)return "#"+tag;
            return "@"+tag;
        }
    }   
