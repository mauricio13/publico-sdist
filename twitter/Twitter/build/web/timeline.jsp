<%-- 
    Document   : timeline
    Created on : Mar 12, 2013, 10:01:22 PM
    Author     : Mauricio
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Objects.Tweet"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="SQLConnection.ConnectDB"%>
<%@page import="util.HTMLCreator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Timeline</title>
    </head>
    <body>
        <h1>Timeline</h1>
        <%
            String un = request.getParameter("email");
            String password = request.getParameter("password");
            String redirectURL = "login.jsp";
            if (un != null && password != null) {
                ConnectDB db = new ConnectDB("localhost",3306,"twitter","root","11111");
                db.open();
                String verifyUser = "SELECT * FROM user WHERE email='"+ un+"' AND password='"+password+"';";
                ResultSet rs = db.query(verifyUser);
                if(rs.next() == false){
                    // If not such user with that password exists return an error message
                    response.sendRedirect(redirectURL);
               }
                else {
                    out.write(HTMLCreator.HTMLParagraphString(un + " timeline"));
                    String user,secString, mentions, followed;
                    int sec = 0;
                    boolean withMentions, withFollowed;
        
                    secString = request.getParameter("sec");
                    mentions = request.getParameter("mentions");
                    followed = request.getParameter("followed");
        
                    sec = (secString != null) ? Integer.parseInt(secString) : 0;
                    withMentions = (mentions == null || mentions.equalsIgnoreCase("YES")) ? true : false;
                    withFollowed = (followed == null || followed.equalsIgnoreCase("YES")) ? true : false;
                    
                    String q = "";
                    String req = "SELECT username FROM user WHERE email='"+un+"';";
                    ResultSet rss = db.query(req);
                    rss.next();
                    String user_name = rss.getString(1);
                    System.out.println(req + " = " + user_name);
                    
                    if(!withMentions && !withFollowed)
                        q = "SELECT * FROM tweet where id_usuario=" + "'" + un + "'ORDER BY tweet_id DESC;" ;
                     else if (!withMentions && withFollowed)
                        q = "SELECT * FROM tweet where id_usuario='" + un + "' OR id_usuario IN (SELECT id_usuario FROM user WHERE username IN (SELECT followed_id from follow where follower_id='"+ user_name +"')) ORDER BY tweet_id DESC;";
                    else if (withMentions && !withFollowed)
                        q = "SELECT * FROM tweet where id_usuario='" + un + "' OR tweet_id IN (SELECT tweet_id FROM at_tag WHERE atuser='"+ user_name +"')ORDER BY tweet_id DESC;";
                    else
                        q = "SELECT * FROM tweet where id_usuario='" + un + "' OR tweet_id IN (SELECT tweet_id FROM at_tag WHERE atuser='"+ user_name +"') OR id_usuario IN (SELECT id_usuario FROM user WHERE username IN (SELECT followed_id from follow where follower_id='"+ user_name +"')) ORDER BY tweet_id DESC;";
                
                    ArrayList <Tweet>tweets = new ArrayList<Tweet>();
                    rss = db.query(q);
                    while (rss.next()) {
                        Tweet t = new Tweet(rss.getInt(1), rss.getString(3),rss.getString(4) );
                        tweets.add(t);                    
                    }
                    
                     out.write(" <table>");
                    for(Tweet tt : tweets){
                        out.println("<tr>");
                        out.print("<td>"+tt.text+"</td>");
                        out.println("</tr>");
                    }
                  db.close();
                out.write(" </table>");
               }
            } else{
                response.sendRedirect(redirectURL);

            }
        %>
    </body>
</html>
