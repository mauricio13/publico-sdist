<%-- 
    Document   : trending
    Created on : Mar 12, 2013, 10:51:26 PM
    Author     : Mauricio
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Objects.HashTag"%>
<%@page import="SQLConnection.ConnectDB"%>
<%@page import="java.util.Date"%>
<%@page import="util.DateRanges"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trending</title>
    </head>
    <body>
        <h1>Trending topics</h1>
        <%
            Date timeframe = null;
            String time;
            time = request.getParameter("time");
            time  = (time == null) ? "day" : time;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println(time);
            if(time.equalsIgnoreCase("day")) timeframe = DateRanges.day();
            else if (time.equalsIgnoreCase("week")) timeframe = DateRanges.week();
            else timeframe = DateRanges.month();
            
            System.out.println(timeframe.toString());
            
            ConnectDB db = new ConnectDB("localhost",3306,"twitter","root","11111");
            db.open();
            String q;
                q = "SELECT * FROM hash_tag WHERE ts >= '" + df.format(timeframe) +"' ORDER BY count DESC, ts DESC LIMIT 0,5";
                ArrayList <HashTag>hashTags = new ArrayList<HashTag>();
                ResultSet rs = db.query(q);
                while (rs.next()) {
                    HashTag h = new HashTag(rs.getString(2), rs.getString(3), rs.getInt(4));
                    hashTags.add(h);   
                }
                
                db.close();
          
                    out.write(" <table>");
                    out.println("<tr>");
                    out.print("<td> Topic </td>");
                    out.print("<td> Mentions </td>");
                   out.println("</tr>");
                    for(HashTag ht : hashTags){
                        out.println("<tr>");
                        out.print("<td>"+ht.count+"</td>");
                        out.print("<td>"+ht.topic+"</td>");
                        out.println("</tr>");
                    }
                  db.close();
                out.write(" </table>");
        %>
    </body>
</html>
