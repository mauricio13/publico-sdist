/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author Mauricio
 */
public class Tweet {
    
    public int id;
    public String text;
    public String email;
    
    public Tweet(int id, String email, String text){
        this.id = id;
        this.text = text;
        this.email = email;
    }
    
}
