package SQLConnection;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mauricio
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectDB {

    String servidor;
    int puerto;
    String DB;
    String user;
    String password;
    Connection conexion;
    
    public ConnectDB(String servidor, int puerto, String DB, String user, String password){
        this.servidor = servidor;
        this.puerto = puerto;
        this.DB = DB;
        this.user = user;
        this.password = password;
            
    }
    
    public Connection open() throws InstantiationException, ClassNotFoundException, IllegalAccessException{
        String URLbaseDeDatos="jdbc:mysql://"+ servidor +":" + puerto + "/" + DB ;
        System.out.println(URLbaseDeDatos);
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        try {
            conexion = DriverManager.getConnection(URLbaseDeDatos,user,"11111");
            return conexion;
        } catch (SQLException ex) {
            return null;
        }
         
    }
    
    public void close() throws SQLException{
        conexion.close();
    }
    
    public ResultSet query(String query){
        PreparedStatement res;
        System.out.println(query);
        try {
            res = conexion.prepareStatement(query);
            return res.executeQuery();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getMessage());
            return null;
        }
            
    }
    
    public boolean updateQuery(String query) throws SQLException{
        PreparedStatement res;
        try {
            res = conexion.prepareStatement(query);
            res.executeUpdate();
            return true;
            
        } catch (SQLException ex) {
            throw ex;
        }
    }
}
