/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import SQLConnection.ConnectDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mauricio
 */
@WebServlet(name = "NewTweet", urlPatterns = {"/NewTweet"})
public class NewTweet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * Handles requests to insert new tweets in the database
     * 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private ConnectDB db;
    
    public NewTweet() throws Exception{
        db = new ConnectDB("localhost",3306,"twitter","root","11111");
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("application/json;charset=UTF-8");
        String un = request.getParameter("user");
        String password = request.getParameter("password");
        String tweet = request.getParameter("tweet");
        PrintWriter out = response.getWriter();
        try {
            if (un == null || tweet == null || password == null) out.write("{\"error\":\"missing arguments\"}");
            else {
                try {
                    db.open();
                } catch (InstantiationException ex) {
                    Logger.getLogger(NewTweet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(NewTweet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(NewTweet.class.getName()).log(Level.SEVERE, null, ex);
                }
                // First check that the user exist and the password corresponds to that user
                String verifyUser = "SELECT * FROM user WHERE email='"+ un+"' AND password='"+password+"';";
                ResultSet rs = db.query(verifyUser);
                if(rs.next() == false){
                    // If not such user with that password exists return an error message
                    out.write("{\"error\":\"invalid username or password\"}");
                } else {
                    // Insert the tweet to the database (the values not provided in the query will be the default ones) and return a success message
                    String insertQ = "INSERT INTO tweet (tweet_id, id_usuario, text ) VALUES(DEFAULT, '" + un +"','" + tweet + "')";
                    db.updateQuery(insertQ);
                    out.write("{\"status\":\"succes\"}");
                }
            }
        } finally {            
            out.close();
            db.close();;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NewTweet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NewTweet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
