/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Objects.HashTag;
import SQLConnection.ConnectDB;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mauricio
 */
@WebServlet(name = "TrendingTopics", urlPatterns = {"/TrendingTopics"})
public class TrendingTopics extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private ConnectDB db;
    
    public TrendingTopics(){
        System.out.println("UserTweets constructor");
        db = new ConnectDB("localhost",3306,"twitter","root","11111");;
    }
    
    public Date day(){
        Date now = new Date();
        Date previousDay = new Date(now.getTime() - 24*60*60*1000 );
        return previousDay;
    }
    
    public Date week(){
        Date now = new Date();
        Date previousWeek = new Date(now.getTime() - 7*24*60*60*1000 );
        return previousWeek;
    }
    
    public Date month(){
        Date now = new Date();
        Date previousMonth = new Date(now.getTime() - 30*60*60*1000 );
        return previousMonth;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        Date timeframe = null;
        String time;
        time = request.getParameter("time");
        time  = (time == null) ? "day" : time;
        
        if(time.equalsIgnoreCase("day")) timeframe = day();
        else if (time.equalsIgnoreCase("week")) timeframe = week();
        else timeframe = month();
        

    
    try {

                try {
                    db.open();
                } catch (InstantiationException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                String q;
                q = "SELECT * FROM hash_tag WHERE ts >= '" + df.format(timeframe) +"' ORDER BY count DESC, ts DESC LIMIT 0,5";
                ArrayList <HashTag>hashTags = new ArrayList<HashTag>();
                ResultSet rs = db.query(q);
                while (rs.next()) {
                    HashTag h = new HashTag(rs.getString(2), rs.getString(3), rs.getInt(4));
                    hashTags.add(h);   
                }
                     
                     out.write(new Gson().toJson(hashTags));
                
        } finally {            
            out.close();
            db.close();
        }
   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
