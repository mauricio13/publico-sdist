/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import SQLConnection.ConnectDB;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import Objects.Tweet;
import SQLConnection.ConnectDB;
import com.google.gson.*;
import java.util.ArrayList;

/**
 *
 * @author Mauricio
 */
public class UserTweets extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     * Handles request to query the tweets database.
     * Dosen't modify the database
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private ConnectDB db;
    
    public UserTweets() throws Exception{
        System.out.println("UserTweets constructor");
        db = new ConnectDB("localhost",3306,"twitter","root","11111");;
    }
    
    private String getUsername(String email) throws SQLException{
        String req = "SELECT username FROM user WHERE email='"+email+"';";
        ResultSet rs = db.query(req);
        if (rs.next())return rs.getString(1);
        return null;
        
    }
    
    protected void processRequest(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("application/json;charset=UTF-8");
        
        System.out.println("=========== REQUEST ======================");
        
        String user,secString, mentions, followed;
        int sec = 0;
        boolean withMentions, withFollowed;
        
        user = request.getParameter("user");
        secString = request.getParameter("sec");
        mentions = request.getParameter("mentions");
        followed = request.getParameter("followed");
        
        sec = (secString != null) ? Integer.parseInt(secString) : 0;
        withMentions = (mentions == null || mentions.equalsIgnoreCase("YES")) ? true : false;
        withFollowed = (followed == null || followed.equalsIgnoreCase("YES")) ? true : false;
        
        
        
        PrintWriter out = response.getWriter();
        try {
            if(user == null) out.write("{Error}");
            else {
                try {
                    db.open();
                } catch (InstantiationException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
                }
                String q = "";
                String user_name = getUsername(user);
                if(!withMentions && !withFollowed)
                    q = "SELECT * FROM tweet where id_usuario=" + "'" + user + "'ORDER BY tweet_id DESC;" ;
                else if (!withMentions && withFollowed)
                    q = "SELECT * FROM tweet where id_usuario='" + user + "' OR id_usuario IN (SELECT id_usuario FROM user WHERE username IN (SELECT followed_id from follow where follower_id='"+ user_name +"')) ORDER BY tweet_id DESC;";
                else if (withMentions && !withFollowed)
                    q = "SELECT * FROM tweet where id_usuario='" + user + "' OR tweet_id IN (SELECT tweet_id FROM at_tag WHERE atuser='"+ user_name +"')ORDER BY tweet_id DESC;";
                else
                    q = "SELECT * FROM tweet where id_usuario='" + user + "' OR tweet_id IN (SELECT tweet_id FROM at_tag WHERE atuser='"+ user_name +"') OR id_usuario IN (SELECT id_usuario FROM user WHERE username IN (SELECT followed_id from follow where follower_id='"+ user_name +"')) ORDER BY tweet_id DESC;";
                
                ArrayList <Tweet>tweets = new ArrayList<Tweet>();
                ResultSet rs = db.query(q);
                while (rs.next()) {
                    Tweet t = new Tweet(rs.getInt(1), rs.getString(3),rs.getString(4) );
                    tweets.add(t);                    
                }
                out.write(new Gson().toJson(tweets));
            }
        } finally {            
            out.close();
            db.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserTweets.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
