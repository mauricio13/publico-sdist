/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Date;

/**
 *
 * @author Mauricio
 */
public class DateRanges {
    
    /**
     *
     * @return
     */
    public static Date day(){
        Date now = new Date();
        Date previousDay = new Date(now.getTime() - 24*60*60*1000 );
        return previousDay;
    }
    
    public static Date week(){
        Date now = new Date();
        Date previousWeek = new Date(now.getTime() - 7*24*60*60*1000 );
        return previousWeek;
    }
    
    /**
     *
     * @return
     */
    public static  Date month(){
        Date now = new Date();
        // This is something weird as fuck, for some reson if I substract one month 
        // to the current time, it ends up adding one month
        Date previousMonth = new Date(now.getTime() + 30*24*60*60*1000 );
        return previousMonth;
    }
    
}
