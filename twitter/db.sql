CREATE DATABASE IF NOT EXISTS twitter;
USE twitter;

DROP TABLE IF EXISTS hash_tag;
DROP TABLE IF EXISTS at_tag;
DROP TABLE IF EXISTS tweet;
DROP TABLE IF EXISTS follow;
DROP TABLE IF EXISTS user;
DROP PROCEDURE IF EXISTS insert_tags;
DROP PROCEDURE IF EXISTS updateHash;
DROP TRIGGER IF EXISTS insTags;

CREATE TABLE user
(
    email varchar(80) primary key,
    username varchar (24) unique key,
    password varchar(20),
    first_name varchar(40),
    last_name varchar (40)
);

CREATE TABLE tweet
(
    tweet_id int primary key AUTO_INCREMENT,
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario varchar(80),
    text varchar(160),
    FOREIGN KEY (id_usuario) REFERENCES user (email)
);

CREATE TABLE follow
(
    follower_id varchar(24) NOT NULL,
    followed_id varchar(24) NOT NULL,
    FOREIGN KEY (follower_id) REFERENCES user (username),
    FOREIGN KEY (followed_id) REFERENCES user (username)
);

CREATE TABLE  at_tag
(
    tweet_id int NOT NULL,
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    atuser varchar(24),
    FOREIGN KEY (tweet_id)  REFERENCES tweet (tweet_id)
);

CREATE TABLE  hash_tag
(
    tweet_id int NOT NULL,
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    hash varchar(40) PRIMARY KEY,
    count int NOT NULL DEFAULT 1,
    FOREIGN KEY (tweet_id)  REFERENCES tweet (tweet_id)
);

DELIMITER $$

#   It updates the count in the hashtag entry if this already exists, otherwise it creates a new one
CREATE PROCEDURE `twitter`.`updateHash` (IN id INT, IN hashVar varchar(40) )
BEGIN
#START TRANSACTION;
        IF EXISTS (SELECT * FROM hash_tag where hash=hashVar) THEN
            UPDATE hash_tag SET count=(count + 1) WHERE hash=hashVar;
        ELSE INSERT hash_tag (tweet_id, hash) VALUES (id, hashVar);
        END IF;
#COMMIT;
END$$

# stored procedure to insert tags (#, @ tags) in the database automatically when a tweet is sent to the server
# the procedure parses the text to find tags and stores them appropriately according to their type (# or @)

CREATE PROCEDURE `twitter`.`insert_tags` (IN tweet_id INT, IN tweet varchar(160) )
BEGIN
    DECLARE length INT;
    DECLARE i INT;
    DECLARE left_index INT;
    DECLARE right_index INT;
    
    SET length = LENGTH(tweet);
    SET i = 1;
    SET left_index = 0;
    set right_index = 0;


    # First locate all the @ tags in the tweet and insert them in the at_tag table of tha database    

    WHILE (i <length && i > 0) DO
        SET left_index = LOCATE('@', tweet, i);
        IF left_index != 0 THEN
            SET right_index = LOCATE(' ', tweet, left_index);
            IF right_index = 0 THEN 
                SET right_index = LOCATE('\n', tweet, left_index);
                IF right_index = 0 THEN 
                    SET right_index = length;
                END IF;
            END IF;
            SET i = right_index + 1;
            INSERT INTO at_tag (tweet_id, atuser) VALUES (tweet_id, SUBSTRING(tweet, left_index + 1, right_index - left_index));
        ELSE SET i = 0;
        END IF;
    END WHILE;

    SET i = 1;
    SET left_index = 0;
    set right_index = 0;

    # Locate the # tags 

    WHILE (i <length && i > 0) DO
        SET left_index = LOCATE('#', tweet, i);
        IF left_index != 0 THEN
            SET right_index = LOCATE(' ', tweet, left_index);
            IF right_index = 0 THEN 
                SET right_index = LOCATE('\n', tweet, left_index);
                IF right_index = 0 THEN 
                    SET right_index = length;
                END IF;
            END IF;
            SET i = right_index + 1;
            # we dont want to insert a new hash tag always, if a hashtaga already exists we only update the entry. We do this in the updateHash method
            CALL updateHash(tweet_id, SUBSTRING(tweet, left_index + 1, right_index - left_index));
#           INSERT INTO hash_tag (tweet_id, hash) VALUES (tweet_id, SUBSTRING(tweet, left_index + 1, right_index - left_index));
        ELSE SET i = 0;
        END IF;
    END WHILE;
END $$

        
DELIMITER ;

# trigger to parse each tweet before insertion
CREATE TRIGGER insTags AFTER INSERT ON tweet
    FOR EACH ROW CALL insert_tags(NEW.tweet_id, NEW.text);
